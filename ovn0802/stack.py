from random import randint
from cylinder import Cylinder


class Stack():
    def __init__(self, n_cylinders):
        self.cylinders = [Cylinder(randint(0, 10), 
                                   randint(0, 5))
                          for _ in range(n_cylinders)]
    
    def get_stack_height(self):
        # TODO: Return the height of the stacked cylinders.
    
    def get_stack_width(self):
        # TODO: Return the width of the tower.
    
    def get_stack_volume(self):
        # TODO: Return the volume of the tower.