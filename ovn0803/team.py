"""
a. Initieras med en lista med ett lagnamn som ska sparas som ett klassattribut.
b. Klassen ska även innehålla data om lagets poäng och målskillnad.
c. Klassen ska ha en metod som kan användas till att uppdatera lagets data 
   efter att en match har spelats. Om laget har vunnit ska poängen öka med 3, 
   om matchen har slutat oavgjort ska poängen öka med 1 och vid förlust ska 
   poängen förbli oförändrad.
"""