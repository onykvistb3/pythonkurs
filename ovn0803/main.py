"""
a. Läsa in de två datafilerna.
b. Initiera en Table-instans med en lista med de lagnamn givna av teams.csv 
   som argument.
c. Iterera igenom alla matcher i PL_1819.csv och lägga till dem en och en i 
   Table-instansen så att denna kan uppdatera sina Team-instanser.
d. Efter var tionde match och efter den sista matchen för säsongen ska Table-
   instansen ombeds att skriva ut hela tabellställningen.
"""