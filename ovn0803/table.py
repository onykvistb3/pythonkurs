"""
a. Initieras med en lista med lagnamn. Utifrån vart och ett av dessa lagnamn 
   ska en Team-instans skapas och läggas i en lista.
b. Ha en metod för att ta emot data från en match och uppdatera de lag som 
   matchen gäller.
c. Kunna skriva ut hela tabellställningen sorterad på poäng i fallande skala i
   formatet Lagnamn, Poäng, Målskillnad.
"""